<a name="unreleased"></a>
## [Unreleased]

### Changes
- Remove privacy alert in command description
- Display a message asking for donations :D

### Features
- Send IMAP ID
- Add Code of Conduct


<a name="2.3.15"></a>
## [2.3.15] - 2024-04-27
### Features
- Keep privacy of users censoring credentials


<a name="2.3.14"></a>
## [2.3.14] - 2024-04-27
### Fixes
- Resolve "Improve/re-do changelog"


[Unreleased]: https://gitlab.com/shackra/goimapnotify/compare/2.3.15...HEAD
[2.3.15]: https://gitlab.com/shackra/goimapnotify/compare/2.3.14...2.3.15
[2.3.14]: https://gitlab.com/shackra/goimapnotify/compare/2.3.13...2.3.14
